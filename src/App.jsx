import './App.css'
import HomePage from './components/HomePage'
import { Routes, Route } from 'react-router-dom'
import DetailsPage from './components/DetailsPage'
import FormPage from './components/FormPage'
import NavBar from './components/NavBar'
import AboutPage from './components/AboutPage'
import ContactPage from './components/ContactPage'

function App() {
  return (
    <div >
    <NavBar/>
      <Routes>
        <Route path="/" exact element={<HomePage />} />
        <Route path="/home" exact element={<HomePage />} />
        <Route path="/blog/:id" exact element={<DetailsPage />} />
        <Route path="/blog/edit/:id" exact element={<FormPage type='Edit' />} />
        <Route path="/new" exact element={<FormPage type='Creat' />} />
        <Route path="/about" exact element={<AboutPage />} />
        <Route path="/contact" exact element={<ContactPage />} />
        <Route path="*" element={<h1>Not Found</h1>} />
      </Routes>
    </div>
  )
}

export default App
