import React from 'react'
import useFetch from './useFetch'
import { useParams } from "react-router-dom";

export default function DetailsPage() {

    const { id } = useParams();
    const { data: blog, error, isPending } = useFetch('http://localhost:8000/blog/' + id);

    return (
        <div className="container">
            <div className="row">
                {isPending && <div>Loading...</div>}
                {error && <div>{error}</div>}
                {blog && (<div className="col-sm-12">
                    <h1 className="page__title my-3 text-center"> {blog.title} &mdash; </h1>
                    <blockquote className="blockquote">
                        <p className="mb-0">{blog.description}</p>
                        <footer className="blockquote-footer text-end"> {blog.authorName} </footer>
                    </blockquote>
                </div>)}
            </div>
        </div>
    )
}
