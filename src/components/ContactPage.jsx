import React from 'react'

export default function ContactPage() {
    return (
        <div className='container my-3'>
            <h1 className='text-center' >Contact</h1>
            <p className='text-center'>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nisi optio minus voluptatibus minima qui, culpa incidunt repellendus consectetur natus ad repellat. Corporis placeat reiciendis, debitis, velit itaque necessitatibus dicta molestiae cum expedita officia temporibus at maxime fugit voluptatem voluptatum, provident voluptate autem consequatur nemo quas natus amet sapiente magnam? Voluptates veritatis hic vero nobis temporibus, quis at aspernatur, nulla illo molestias, quidem harum et magni quae. Culpa, ab nulla. Exercitationem voluptatum iste at nemo cupiditate. Suscipit consequatur sunt rerum expedita, mollitia ex asperiores, obcaecati distinctio commodi voluptatem, quisquam iusto quam? Sapiente maiores mollitia quidem cupiditate a, itaque quas nisi fugit, iure ratione repellendus atque, nam sint. Veniam, corrupti harum! Culpa vel quis eligendi odio perspiciatis, adipisci nulla. Expedita blanditiis soluta facilis aliquid, illo aspernatur ex, nobis magnam fugit, repudiandae quae laudantium maxime quisquam asperiores animi nisi quibusdam suscipit sed. Repudiandae, nostrum. Quod incidunt natus at vitae in ipsum qui quis, asperiores officiis ducimus cumque fugiat harum sunt consectetur enim molestias deserunt odit consequatur nesciunt velit ut illo corporis voluptate quaerat! Quos incidunt assumenda similique, nemo asperiores nisi accusantium tempora vero aliquam eveniet fugit sunt vitae. Tempora incidunt quam esse ullam sit animi deleniti distinctio dolores amet, vitae adipisci consequuntur autem!
            </p>
        </div>
    )
}
