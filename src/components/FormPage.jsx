import React from 'react'
import useFetch from './useFetch'
import { useNavigate, useParams } from "react-router-dom";

export default function FormPage(props) {

    const [blog, setBlog] = React.useState({ title: '', description: '', authorName: '' });
    const { id } = useParams();
    const navigate = useNavigate();

    React.useEffect(() => {
        if (id)
        {
            fetch('http://localhost:8000/blog/' + id)
                .then(res => res.json())
                .then(data => setBlog(data));
        } 
    }, [id]);

    const handleSubmit = (e) => {
        e.preventDefault();

        if (props.type === 'Creat') {
            fetch('http://localhost:8000/blog/', {
                method: 'POST',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(blog)
            }).then(() => {
                navigate('/');
            })
        } else {
            fetch('http://localhost:8000/blog/' + id, {
                method: 'PUT',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(blog)
            }).then(() => {
                navigate('/blog/' + id);
            })
        }
    }

    return (
        <div className="container contact-form">
            <form method="post">
                <h3>{props.type}ing Data</h3>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group m-3">
                            <input type="text" name="title" className="form-control" placeholder="Title *" value={blog.title} onChange={e => { setBlog({ ...blog, title: e.target.value }) }} />
                        </div>
                        <div className="form-group m-3">
                            <input type="text" name="authorName" className="form-control" placeholder="Author Name *" value={blog.authorName} onChange={e => { setBlog({ ...blog, authorName: e.target.value }) }} />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group m-3">
                            <textarea name="body" className="form-control" placeholder="Description *" value={blog.description} onChange={e => { setBlog({ ...blog, description: e.target.value }) }}></textarea>
                        </div>
                        <div className="form-group m-3">
                            <input type="submit" name="btnSubmit" className="btnContact" value="Send Message" onClick={handleSubmit} />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}
