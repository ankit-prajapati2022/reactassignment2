import React from 'react'
import { Link } from 'react-router-dom'
import useFetch from './useFetch'

export default function HomePage() {

    const { error, isPending, data: blogs } = useFetch('http://localhost:8000/blog')

    const handleDelete = (bid) => {
        fetch('http://localhost:8000/blog/' + bid, {
            method: 'DELETE'
        }).then(() => {
            window.location.reload(false)
        })
    }

    const makecard = (blog) => {
        return (
            blog.map(blog => {
                return (
                    <div className="col-md-4 col-sm-6 content-card" key={blog.id}>
                        <div className="card-big-shadow">
                            <div className="card card-just-text" >
                                <div className="content">
                                    <h4 className="title">{blog.title}</h4>
                                    <h6 className="category mb-2">- {blog.authorName}</h6>
                                    <p className="description">{blog.description.slice(0, 150)} <br /> ... Read More </p>
                                    <div className="btn-group" role="group">
                                        <Link to={`/blog/${blog.id}`} type="button" className="btn btn-success">Show</Link>
                                        <Link to={`/blog/edit/${blog.id}`} type="button" className="btn btn-warning">Edit</Link>
                                        <button type="button" className="btn btn-danger" onClick={() => handleDelete(blog.id)} >Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        )
    }

    return (
        <div className='homepage'>
            <div className="container">
                <div className="row justify-content-around">
                    {error && <div>{error}</div>}
                    {isPending && <div>Loading...</div>}
                    {blogs && makecard(blogs)}
                </div>
            </div>
        </div>
    )
}
